const c = require('./config');

class Router {
    #hostname;
    #port;
    #eventEmitter;
    #routes;


    constructor(config) {
        this.#hostname = config.hostname;
        this.#port = config.port;
        this.#eventEmitter = config.eventEmitter;

        this.#routes = {};
        this.#setRequestListener();
    }

    addRoute(routeObj) {
        if (!this.#routes.hasOwnProperty(routeObj.route)) {
            this.#routes[routeObj.route] = {};
        }
        this.#routes[routeObj.route][routeObj.method] = {
            function: routeObj.function,
            middlewares: routeObj.middlewares
        };
    }

    #route(req, res) {
        // console.log("--------------hitRoute----------------")
        // console.log(hitRoute);
        // console.log("--------------thisRoutes----------------")
        // console.dir(this.#routes, {depth: null});
        const reqUrlMinusQs = req.url.split('?')[0];
        let hitRoute = this.#routeHit(reqUrlMinusQs, this.#routes);
        let hitRoutePath = hitRoute.route;
        if (!(hitRoute.route) || !(this.#routes[hitRoutePath].hasOwnProperty(req.method))) {
            console.log(`${c.errors.routeNotFound.code}: ${c.errors.routeNotFound.message} -> ${req.method} ${req.url} ${hitRoute}`);
            this.#sendNotFoundResponse(res);
            return;
        }
        
        ////adding Array inside req for further use
        req.urlParams = hitRoute.urlParams;
        req.urlParamsObj = hitRoute.urlParamsObj;
        
        let middlewares = this.#routes[hitRoutePath][req.method].middlewares;
        let handler = this.#routes[hitRoutePath][req.method].function;

        let firstMiddleware = prepareMiddlewares(middlewares.length - 1, req, res, () => handler(req, res));
        firstMiddleware();

        function prepareMiddlewares(index, req, res, next) {
            if (index === -1)
                return next;
            return prepareMiddlewares(index - 1, req, res, () => middlewares[index](req, res, next));
        }


    }

    #setRequestListener() {
        this.#eventEmitter.on('newReq', (req, res) => {
            this.#route(req, res);
        });
    }

    #sendNotFoundResponse(res) {
        res.stasusCode = c.statusCodes.NOT_FOUND_STATUS_CODE;
        res.setHeader('Content-Type', c.contentTypes.JSON);
        res.end(JSON.stringify(c.errors.routeNotFound));
    }

    /*
    * look for inputUrl inside routes
    * @param inputUrl - String req.rul
    * @param routes - Object routes
    * @return - Object hitRoute - FALSE if not found
    * e.g: http://127.0.0.1:65432/user/2
    * hitRoute.route = <String - '/user/:id'>
    * hitRoute.urlParams = Array - [null, null, 2]
    * take note the first / counts as 0 index
    */

    #routeHit(inputUrl, routes) {
        console.log({inputUrl});
        let hitRoute = {};
        hitRoute.route = '';
        hitRoute.urlParams = [];
        hitRoute.urlParamsObj = {};
        let splitInputUrl = inputUrl.split('/');
        let compare;
        for (let route in routes) {
            let splitRoute = route.split('/');
            compare = true;
            if (splitInputUrl.length === splitRoute.length) {
                for (let i = 0; i < splitInputUrl.length; ++i) {
                    if (splitRoute[i][0] !== ':' && splitInputUrl[i] !== splitRoute[i]) {
                        compare = false;
                        break;
                    } else if (splitRoute[i].charAt('0') === ':') {
                        hitRoute.urlParams[i] = splitInputUrl[i];
                        hitRoute.urlParamsObj[splitRoute[i].substring(1)] = splitInputUrl[i];
                    }
                }
                if (compare === true) {
                    hitRoute.route = route;
                    console.log({hitRoute});
                    return hitRoute;
                }
            }
        }
        return false;
    }

}

module.exports = Router;