const statusCodes = {
  NOT_FOUND_STATUS_CODE: 404
};

const contentTypes = {
  JSON: 'application/json'
};

const errors = {
  routeNotFound: {
    code: 'routeNotFound',
    message: 'Requested route/method not found!'
  }
};

module.exports = {
  statusCodes,
  contentTypes,
  errors
};